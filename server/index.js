const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http, { cors: {
    origin: "http://localhost:4000",
    methods: ["GET", "POST"]
}});
 
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
 
http.listen(3000, function(){
    console.log('HTTP server started on port 3000');
})
 
const timeout = 500

setInterval(async function() {
        const data = getdata()

        //Wait a random time so it feels more realistic
        await new Promise(r => setTimeout(r, Math.floor(Math.random()*2000)));

        console.log("data",data);

        io.emit('data', data);
    
}, timeout);

const assets = [
    {"name":"Bitcoin","asset":"XBT","price":18000.00,"volume":1000},     
    {"name":"Ethereum","asset":"ETH","price":570.00,"volume":1000},
    {"name":"Ripple","asset":"XRP","price":0.556,"volume":1000},
    {"name":"Tether USD","asset":"USDT","price":0.99,"volume":1000},
    {"name":"Litecoin","asset":"LTC","price":76.00,"volume":1000},
    {"name":"Bitcoin Cash","asset":"BCH","price":266.2,"volume":1000},
    {"name":"Chainlink","asset":"LNK","price":12.266,"volume":1000},
    {"name":"Cardano","asset":"ADA","price":0.143,"volume":1000},
    {"name":"Polkadot","asset":"DOT","price":4.917,"volume":1000},
    {"name":"Lumen","asset":"XLM","price":0.16,"volume":1000},
    {"name":"EOS","asset":"EOS","price":2.79,"volume":1000},
    {"name":"Monero","asset":"XMR","price":131.79,"volume":1000},
    {"name":"TRON","asset":"TRX","price":0.0284,"volume":1000},
    {"name":"Tezos","asset":"XTZ","price":2.16,"volume":1000}
]   

function getdata(){
    const assetNumber = Math.floor(Math.random() * assets.length)
    const asset = assets[assetNumber]
    const variance = (Math.random()-0.48)*asset.price;
    const newPrice = asset.price + variance;
    const spread = (Math.random())/10 * newPrice;
    const bid = newPrice - spread;
    const ask = newPrice + spread;
    const volumeVariance = (Math.random()-0.48)*asset.volume;
    let newVolume = Math.floor(asset.volume + volumeVariance)

    if(newVolume < 1.0){
        newVolume = Math.abs(newVolume)
    }
    
    const update = {"name":asset.name,"asset":asset.asset,"bid":bid,"ask":ask,"lastTrade":newPrice,"volume":newVolume}
    assets[assetNumber]={"name":asset.name,"asset":asset.asset,"price":newPrice,"volume":newVolume}

    return update;
}

