# Feeder

Simple dummy trade feed application

## Installing the solution

```
npm install
```

## Run the server

```
npm run server
```

## Run the client

open another terminal and then

```
npm run client
```

## View the application

Go to the following address

```
http://localhost:4000
```

You should see a feed of crypto assets and their prices.
