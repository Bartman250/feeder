const { assert } = require('console');

//const cors = require('cors');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http, { cors: {
    origin: "http://localhost:4000",
    methods: ["GET", "POST"]
  }});
 
app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});
 
http.listen(4000, function(){
    console.log('HTTP server started on port 4000');
})